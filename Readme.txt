# Instalar virtual env
pip install virtual env

# Criação do ambiente virtual
virtualenv nome_do_ambiente_virtual --python=versão_do_python

# Ativação do ambiente virtual
source nome_da_virtualenv/bin/activate (Linux ou MacOs)
"nome_da_virtualenv/Scripts/Activate" (Windows)

