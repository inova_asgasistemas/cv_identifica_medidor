# Importando os módulos necessários
import os
import cv2
import numpy as np
from skimage.measure import label, regionprops
from skimage.color import label2rgb
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


print(cv2.__version__)
print(np.__version__)

imagem = cv2.imread("C:\\Users\\adailton.holanda\\Desktop\\Launch Base\\Medidor\\POC1\\testar_rec\\1.png")
cv2.imshow(" ", imagem)
cv2.waitKey(0)
cv2.destroyAllWindows()

imagem_hsv = cv2.cvtColor(imagem, cv2.COLOR_BGR2HSV)
h, s, v = cv2.split(imagem_hsv)

clahe = cv2.createCLAHE(clipLimit=5.0, tileGridSize=(10,10))
gray_clahe = clahe.apply(v)

kernel = np.ones((15,15),np.uint8)
gradient = cv2.morphologyEx(gray_clahe, cv2.MORPH_BLACKHAT, kernel)

